FROM alpine

RUN apk add --update nodejs \
    && apk add --update npm \
    && rm -rf /var/cache/apk/* \
    && apk --no-cache add curl 


WORKDIR /usr/src/app

COPY . .

WORKDIR /usr/src/app/node
RUN npm install

CMD [ "npm", "start" ]