const express = require('express');
const app = express();
const https = require('https');
const fs = require('fs');
const webpush = require('web-push');
const apikeys = require('./apikeys');

const options = {
    key: fs.readFileSync('SSL/private.key'),
    cert: fs.readFileSync('SSL/private.crt')
};

const bodyParser = require('body-parser')
app.use(bodyParser.json());

const ArrayOfEndpoints = [];

webpush.setVapidDetails(
    apikeys.mail,
    apikeys.publicKey,
    apikeys.privateKey
)

app.get('/', function (req, res) {
    res.send('Push app!')
});

app.use(express.static('../public'))

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

app.post('/api/save-subscription/',((req, res) => {
    ArrayOfEndpoints.push(req.body);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({data: {success: true}}))
    console.log(ArrayOfEndpoints);
    return res;
}));

app.get('/api/send/',((req, res) => {
    console.log('Send notif...');
    ArrayOfEndpoints.forEach(endpoint => {
        console.log(endpoint)
        webpush.sendNotification(endpoint, JSON.stringify({ title: "Test", body: "This is my body", icon: "https://www.mis.u-picardie.fr/sites/default/files/inline-images/Logo-UPJV-bleu.png" })).catch((e) => { console.log(e) });
    });
    res.sendStatus(202);
}));


https.createServer(options, app).listen(443, () => {
    console.log('Server public listening on port 443');
});