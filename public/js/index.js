const checkBrowser1 = checkBrowser();

document.addEventListener("DOMContentLoaded", function() {
    main();
});

function main() {
    if (!checkBrowser1) {
        return;
    }

    askNotificationPermission();

    console.log(checkBrowser1);
}