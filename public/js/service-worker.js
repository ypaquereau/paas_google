self.addEventListener('push', function(event){
    let msg = event.data.json()
    const promiseChain = self.registration.showNotification(msg.title, {body: msg.body, icon: msg.icon});

    event.waitUntil(promiseChain)
});