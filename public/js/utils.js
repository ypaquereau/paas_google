const PUBLIC_KEY="BDeWmL35YOIvasXjGVHpchO9Nc6sxnURMnfhMeleE-pm5lktDdnZOJdNeFJ6xrNsRRIHAdwDA6H0yXHp_Erh8RA";

let req = new XMLHttpRequest();

function checkBrowser() {
    return (('serviceWorker') in navigator && ('PushManager' in window));
}

function registerServiceWorker() {
    return navigator.serviceWorker.register('js/service-worker.js')
        .then(function (registration) {
           console.log('Service worker successfully registered')

            const options = {
                userVisibleOnly: true,
                applicationServerKey: urlBase64ToUint8Array(PUBLIC_KEY)
            };
            registration.pushManager.subscribe(options).then(
                function(pushSubscription) {
                    console.log(pushSubscription.endpoint);
                    req.open("POST", '/api/save-subscription')
                    req.setRequestHeader('Content-Type', 'application/json');
                    req.send(JSON.stringify(pushSubscription))
                    // The push subscription details needed by the application
                    // server are now available, and can be sent to it using,
                    // for example, an XMLHttpRequest.
                }, function(error) {
                    // During development it often helps to log errors to the
                    // console. In a production environment it might make sense to
                    // also report information about errors back to the
                    // application server.
                    console.log(error);
                }
            );

           return registration;
        }).catch(function(error) {
            console.log('Registration failed with ' + error);
        });
}

/**
 * Conversion de la clef VAPID pour la subscription
 * @param base64String
 * @returns {Uint8Array}
 */
function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

function askNotificationPermission() {
    let notificationCheckElm = document.getElementById('notification-check');
    let sendNotificationElm = document.getElementById('send-notification');

    if (Notification.permission === "granted") {
        notificationCheckElm.innerText = "Notification autorisée";
        sendNotificationElm.innerHTML = "<button>Envoyer une notification</button>";
        let se_registration = registerServiceWorker();
        console.log(se_registration);


        document.querySelector('#send-notification > button').addEventListener('click', () => {
            console.log('Send notif...');
            req.open("GET", '/api/send');
            req.setRequestHeader('Content-Type', 'application/json');
            req.send();
        });
        
    } else if (Notification.permission === "default") {
        notificationCheckElm.innerHTML = "Par défaut <button id='ask-notification'>Activer</button>";
        let askNotificationButton = document.getElementById('ask-notification');
        if (askNotificationButton !== null) {
            document.getElementById('ask-notification').addEventListener('click', active);
        }
    } else if (Notification.permission === "denied") {
        notificationCheckElm.innerHTML = "T'es relou";
    }
}

function active() {
    console.log('toto')
    Notification.requestPermission().then(result => {
       if (result === 'granted') {
           let se_registration = registerServiceWorker();
           console.log(se_registration);
       }

       console.log(result);
    });
}